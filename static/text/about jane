An Introduction to Jane

 Jane, the variations on a theme

 The variations on a theme in Jane

 Jane: themes and structure

 The Elements of Jane.

 Jane and her many worlds.

 The Many Worlds of Jane.

 Jane and her many forms.

All About Jane.
 Form and Variation in Jane

Variation through Form In Jane




I wrote Jane in 2007 and managed to shoot it in the same year at the University of Kingston in the UK. We had a really skeleton
crew with only three others apart from the actors and myself: a Director of Photography in Niclas Reed Middleton, Donald Boosted as thesound recordist, and
Sheena Mckellar doing the make-up.

There are various reasons why it took 8 years to finish. Work of this polyvalent nature, the pool of available media multiplies in proportion to the range of choices you provide.
And in 'Jane', there are many. Editing it proved problemmatic and I effectively had
to do it two or even three times over. Considering that Jane is made up of around 1800 separate clips, this alone was almost a bridge too far.
In the end I wrote the selection and playback engine in several languages: mySQL, C++, Actionscript and
finally in Javascript and Python. To a certain degree, this was due to the long slow demise of Adobe Flash as a viable platform for
web development. Then there was illness, moving to another country, and of course, the unrelenting necessity of work.


'Jane' is a dialogue for two actors that you can change throughout while maintaining credible meaning. There is no definitive version, no directors cut.
It is rather the sum of its manifold possibilities. The work in many ways belongs to the theatre with its black box setting.
And it owes probably more to work of Samuel Beckett than anyone else. At its strange heart the piece is a writing automaton. It chooses
what the algorithms decide for it against the carefully calculated script.

The situation is absurd. Two men talk about a woman they share
called Jane. Both, more than coincidentally, are married to a woman called Jill.

I wrote the script, and then produced it in 2007 at Kingston University in the UK. Note the rather long hiatus between that and its
final release on the web in late 2015.

John and Jake exhibit no rivalry whatsoever over Jane. They know Jane belongs to everyone and no one.  She is simultaneously a daydream and a nightmare.
Shape-shifter extraordinaire, she is the constantly mutating remainder of male desire. Whatever men crave and fear in the female sex, Jane
exemplifies and amplifies. In a very concrete sense, Jane could only be expressed in something as fluid and pliable as the work that takes her name.
John and Jake too are rather ambivalent. They can be read as individuals or the same person separated by the attrition of time. Taken as
individuals, John is the older, more jaundiced and stoic of the pair. He is also probably happier or at least more accepting of his marriage and its inevitable
compromises. Jake is the more idealistic and yet the more troubled too. He bears the burden of a sexual ambiguity both within himself and from the troubled relation
his wife has with her father. Originally I had in fact planned a companion piece called 'James' where the wives called Jill would similarly fantasize and fear. There I was anticipating
a festival of male to female interweavings.


'Jane' is also about itself or rather the types of story that something like 'Jane' can tell. I tried to get the most out of the
line by line granularity of change by investing the alternatives with as much narrative content as they could hold. One way was to use the
well known films in the conversation 'Old Films, New Endings' and thereby leverage, extend and subvert narratives already known
by many. The conversation 'Bedtime Stories' does much the same, and harkens back to our childhood need for night time
narrative that like cinema unfolds itself in the dark. In 'Filmic Fantasies', I evoke actors throughout and use their names to conjure the clouds of narrative
 allusion associated with each. And then there is Jane. Of her occupation we know that it was once something cinematic: director, actress,
 editor, and auteur.

In other interactive stories that are varieties of choose-your-own-adventure, the main narrative branches at key decision points. Progression then leads to a normally limited set
of outcomes. I eschew this approach and use instead variants that have localized polyvalence. This creates a cumulative field of multiplying nuance where
one change can directly or indirectly affect the others.
This does mean that the narratives I produce are less directed than other approaches. The films you see in 'Jane' are therefore more like vignettes than conventional stories as such.
They are the equivalents of the groundhog day conversations we engage in daily with others or ourselves and upon which we gently improvise as a meagre exercise of will.

 The definite structure to Jane amounts to a rudimentary generative logic behind it. Each conversation has three parts of three keywords spoken near the initial pause of variable length, namely 'last might', 'fantasy'
 and 'tonight. Each conversation begins with either Jake or John speaking the keyword. The resulting six conversations got titles much later but in
 the initial script, I wrote the following conversations in this order:

 1. Last Night Jake - Old Films, New Endings
 2. Last Night John - Bedtime Stories
 3. Fantasy Jake    - Married Too Long
 4. Fantasy John    - Sexual Conundrum
 5. Tonight Jake    - Eat In Eat Out
 6. Tonight John    - Filmic Fantasies

 In the web incarnation of the piece you can play one conversation at a time. However, in the forthcoming gallery version, there are
 other combinations of film available.

 You can concatenate a sequence via keyword part from 3 up to 6 parts. For example, you could choose to have six fantasy sections, or three tonight, one
 last night and 1 fantasy.

 There is also a weekday cycle of seven exchanges of three keyword parts taken across all conversations.
 Starting on Sunday, each day begins with Jake or John asking in turns, 'Do you know what tonight is?'. The other
 can then respond with the next day of the week. Two more keyword parts are played and at their end, we fade to black where a variety of noises can be heard.
 Fade up on the next day, and it continues until the start of the next Sunday where the cycle ends.


For the record here are all sixteen parts that Jane is made of with a descriptive summary of their contents:

 1. Last Night Jake - Old Films, New Endings

      last night: John watched a film on TV last night.
      fantasy: John makes up different endings to the film he watched.
      tonight: Jane recommends an interesting film.

 2. Last Night John - Bedtime Stories

      last night: Jake's wife tells him bedtime stories under certain circumstances.
      tonight: Jake describes the kind of bedtime stories his wife tells him.
      fantasy: The kind of stories that Jane tells.


 3. Fantasy Jake - Married Too Long

       fantasy: John talks about the dream life he would like to lead.
       last night: John has an argument with his wife.
       tonight: John resolves to work things out with his wife.


 4. Fantasy John - Sexual Conundrum

        fantasy: While John is jealous of Jake's married life, Jake is not so sure why.
        tonight: Jane, her gay entourage and Jake's sexuality.
        last night: Jake seeks a solution to his double life with Jane and his wife.

 5. Tonight Jake - Eat In Eat Out

        tonight: Jake has dinner cooked for him again tonight.
        fantasy: John warns Jake about the perils of kitchen politics.
        last night: Farting and going to restaurants with Jane.

 6. Tonight John - Filmic Fantasies
        tonight: John watches TV with his wife in silence, while Jake talks with his.
        fantasy: John observes his wife's televisual fantasy. Jake cannot talk to Jane.
        last night: John's wife calls out for another man and the allure of screen sirens.

        

 There are different types of line variation in 'Jane'. The basic ones are:

 Default

 These are lines that are fixed in dialogue content affording as elsewhere only a change in
  camera angle. They are like anchors of stability in the overall flux surrounding them.



 Free

 This is a group of single lines from which one can be chosen without other lines in the script changing.


 Parent

 This is a group of groups. Parent alternatives can have different types of children such as free, and compound.
 Line selection in a child becomes the selection for the parent.


  Compound

  This is a default line followed by a group of free alternatives and are found most commonly in parents. When outside a
  parent it is simply a default followed by a free on the top-level of the script.


  Paired

  In paired varieties of free and parent, two or more groups of lines are linked. Changing a selection in one of the groups changes
  the corresponding choice in the other linked groups. Pair groups normally have the same number of options so choosing  for example the third option will
  change all the other third placed options. Free groups can pair with parents and vice versa.


  Sequence sets

  A sequence group consists of a library of source material. Different selections from the library are divided into different categories, or
  parent groups to aide algorithmic or user selection.
  The variable pause that starts each keyword section is such a sequence group. Here there is a library of shots belonging to the pause.
  I then chose the various viable combinations of these and put them into categories or sets based on their duration - short, medium or long.


  Loop

  This form of dialogue design is somewhat separate from the rest of Jane as it constitutes a self-enclosed unit of interaction
  quite unto itself. There is only one loop in 'Jane' and the last conversation 'Filmic Fantasies' ends with it.

  A loop here means a self-contained sequence of lines that can be spoken by either actor. Change the speaker of one line and the rest of the lines
  also change for the other. Here the role reversal seen in disparate lines elsewhere is given centre ground and made entirely explicit.
 The exchange begins with John and Jake playing a kind of tennis with actress names. They have have 10 screen sirens apiece to choose from.
 There is an minimum of two such selections between them up to a maxiumum of six. The subsequent lines can be performed by either actor depending on
  who finishes the name exchange. The remaining five lines in the loop have identical content for both characters. In the end either
  John or Jake recovers from either crying or hysterical laughter.


  Camera angles are mostly available throughout. These are either near or far. There are in fact two types of near camera angles per actor: over the shoulder
  of the other, and a tighter head shot. This allows a user to choose two near camera angles for a protagonist in adjacent lines. Likewise there
  are three wide shots where both actors are visible: one angled towards John, another towards Jake, and one straight ahead
  that privileges neither.


  ===========================================

  Ian Flitman is a English media artist.
  His work is concerned with open narrative or stories that have multiple paths through their content.
   
  With a grounding in 16 mm filmmaking techniques,
  This began in 'Hackney Girl' (2003), a video wall diary about his moving from London to Istanbul. Lasting between 12 and 18 minutes,
  this reflection on place, identity

  no two versions of it are the same.


  He has released three works since 2003



  He currently works as a computer scientist for Ecole Polytechnique Federale di Lausanne in Switzerland.







