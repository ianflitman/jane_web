/**
 * Created by ian on 06.02.15.
 */

var janeControllers = angular.module('janeControllers', []);

janeControllers.controller('MasterScriptController',[ '$scope', '$rootScope', '$http', 'Scene', 'PairColour','initData',
    function ($scope, $rootScope, $http, Scene, PairColour, initData) {
        var conversations = ['mtl', 'sc', 'ofne', 'bs', 'eieo', 'ff'];
        var conversationChoice = conversations[Math.floor(Math.random() * conversations.length)];
        console.log('random conversation: ' + conversationChoice);
        var data = Scene.query({scene: conversationChoice});

        data.$promise.then(function (data) {
            $scope.content = function () {
                return data.content;
            };

            $scope.getPartName = function (which) {
                if (which > data.scene.parts.length - 1) {
                    which = 0;
                }
                return data.scene.parts[which].name;
            };

            $scope.title = data.title;
            $scope.generated =  new Date().toUTCString();
            $rootScope.$broadcast('conversationGenerated', conversationChoice);

        });

        $scope.$on('changeConversation', function(e, data) {
            data = Scene.query({scene: data});
            data.$promise.then(function (data) {
                PairColour.reset();
                initData.changed = false;
                $scope.content = function () {
                    return data.content;
                };
            })
        });
    }
]);

janeControllers.controller('ChangesController', ['$rootScope',
    function($rootScope){
        $rootScope.cameraChanges = 0;
        $rootScope.dialogueChanges = 0;
    }
]);

janeControllers.controller('NavCtrl', ['$scope', '$location',function($scope, $location) {

    $scope.goPage = function (page) {
        $location.path('/jane');
    }
}]);

janeControllers.controller('CaptchaCtrl', ['$scope','vcRecaptchaService','$http',
    function ($scope, vcRecaptchaService, $http) {
        $scope.showFeedback=false;
        $scope.email = '';
        $scope.subject= '';
        $scope.message = '';
        $scope.publickey = '6LdLOhATAAAAAE2QC-BpE1FOBUlXQZGoP0UbIDe_';
        $scope.checkEmail = function(){
            console.log('checking email');
            if(vcRecaptchaService.getResponse() === "") { //if string is empty
                alert("Please answer the captcha question before sending")
            }else{
                var post_data = {
                    'email': $scope.email,
                    'subject': $scope.subject,
                    'message': $scope.message,
                    'g-recaptcha-response': vcRecaptchaService.getResponse()
                }
                console.log(vcRecaptchaService.getResponse());

                var $feedback = $('#emailFeedback');

                $http.post('/api/email',post_data).success(function(response){
                    console.log('response: ' + response);
                    if(response === '0'){
                        $feedback.addClass('alert-success');
                        $feedback.text("The email was successfully sent.");
                        $scope.showFeedback = true;
                    }else{
                        $feedback.addClass('alert-danger');
                        $feedback.text("Something went wrong so the email was not sent.");
                        $scope.showFeedback = true;
                    }
                }).error(function(error){
                    $feedback.addClass('alert-danger');
                    $feedback.text("Something went wrong so the email was not sent.");
                    $scope.showFeedback = true;
                })
            }
        }
    }
]);

janeControllers.controller('TabCtrl', function($scope, $window){
    $scope.ofne_1 = 'f_MnubWCiCU';
    $scope.ofne_2 = 'AUTNIsM9KWM';
    $scope.ofne_3 = 'Ys-oyUANOJ4';
    $scope.bs_1 = 'pGyjXtbTpo0';
    $scope.bs_2 = 'kAYMoYzUjIE';
    $scope.bs_3 = 'JFmoMf9F6Tk';
    $scope.mtl_1 = 'VDL7EvPwqew';
    $scope.mtl_2 = 'uZcFzhNIemU';
    $scope.mtl_3 = 'LdwBGMQ_xFU';
    $scope.sc_1 = 'bztJGIwqFFU';
    $scope.sc_2 = 'qQQe8f78mv4';
    $scope.sc_3 = 'o4m5BSRPnbs';
    $scope.eieo_1 = 'l7yLJTcnOTE';
    $scope.eieo_2 = 'odNZrUUGN48';
    $scope.eieo_3 = 'uTpeX6rGfBE';
    $scope.ff_1 = 'UAoYdX0ptUs';
    $scope.ff_2 = 'Q-Csp-ntJPc';
    $scope.ff_3 = 'uNePSXdcnj8';
});

janeControllers.controller('VideoCtrl', ['$sce','$scope', '$timeout', 'initData', 'Play', function($sce, $scope, $timeout, initData, Play ){
    var controller = this;
    $scope.poster = 'images/2a.png';

    controller.API = null;
    controller.onPlayerReady = function(API) {
        console.log('player ready..');
        controller.API = API;

        //controller.config.sources =[{src: $sce.trustAsResourceUrl("http://178.79.184.214/output.mp4"), type: "video/mp4"}];
        //$timeout(controller.API.play.bind(controller.API), 100);
    };

    $scope.movieGenerated = function(val){
        //return val;
        return initData.ready;
    };

    $scope.vgError = function(event) {
     console.log("onVgError: " + event);
    };

    $scope.$on('getPoster', function(event, shot){
        var imgStr  = 'images/' + shot + '.jpg';
        controller.config.plugins = {poster: imgStr};
        $scope.$apply();
    });

    $scope.$on('rebuild', function(event){
        Play()
    });

    $scope.$on('movieReady', function(event,movieName, autoPlay){
        console.log('heard that ' + movieName + ' is ready to play in VideoCtrl!');
        var movieUrl = "http://178.79.184.214/" + initData.UID + "/" + movieName + ".mp4";
        controller.config.sources =[{src: $sce.trustAsResourceUrl(movieUrl), type: "video/mp4"}];
        if(autoPlay)$timeout(controller.API.play.bind(controller.API), 100);
        //$('#videoPlayer').css('display', 'inline-block');
    });

    this.config = {
        sources: [
            {src: $sce.trustAsResourceUrl("http://178.79.184.214/black.mp4"), type: "video/mp4"}//,
            //{src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.webm"), type: "video/webm"},
            //{src: $sce.trustAsResourceUrl("http://static.videogular.com/assets/videos/videogular.ogg"), type: "video/ogg"}
        ],
        theme: "vg/videogular-themes-default/videogular.css"
        /*plugins: {
            poster:'images/co.png'
        }*/
    };
}]);

